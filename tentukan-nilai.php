<?php
function tentukan_nilai($number)
{
	$result = "";
    if($number >= 85 && $number <= 100){
    	$result = "Nilai Sangat Baik";
    }
    else if($number >= 70 && $number < 85){
    	$result = "Nilai Baik";
    }
    else if($number >= 60 && $number < 70){
    	$result = "Nilai Cukup";
    }
    else {
    	$result = "Nilai Kurang";
    }
    return $result;
}

//TEST CASES
echo tentukan_nilai(98)."<br>"; //Sangat Baik
echo tentukan_nilai(76)."<br>"; //Baik
echo tentukan_nilai(67)."<br>"; //Cukup
echo tentukan_nilai(43)."<br>"; //Kurang
?>